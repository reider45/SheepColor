package me.reider45.SheepColor;

import java.util.Random;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	private ItemStack shearItem;
	
	public void onEnable()
	{
		
		// Create our custom item
		shearItem = new ItemStack(Material.SHEARS, 1);

		ItemMeta m = shearItem.getItemMeta();
		m.setDisplayName("�eSheep Colorer");
		shearItem.setItemMeta(m);

		Bukkit.getPluginManager().registerEvents(this, this);

		System.out.println("[SheepColor] Enabled");

	}

	@EventHandler
	public void onTouch(PlayerInteractEntityEvent e)
	{

		if(e.getRightClicked() instanceof Sheep)
		{
			
			Player p = e.getPlayer();

			if(p.getItemInHand() != null)
			{

				if(!p.getItemInHand().equals(shearItem)){ return; }
				
				e.setCancelled(true);
				Sheep s = (Sheep) e.getRightClicked();
				
				// Generate a random color
				Random r = new Random();
				s.setColor(DyeColor.getByData((byte) r.nextInt(16)));

			}

		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player p = (Player) sender;
			
			if(cmd.getName().equalsIgnoreCase("sheep"))
			{
				p.sendMessage(ChatColor.YELLOW + "Use the shears to color sheep!");
				p.getInventory().addItem(shearItem);
			}
			
		}
		return false;
	}


}
